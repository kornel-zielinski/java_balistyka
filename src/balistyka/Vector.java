package balistyka;

//Prosta implementacja trojwymiarowego wektora
public class Vector {
	public double x;
	public double y;
	public double z;
	
	public Vector() {x=0; y=0; z=0;}
	public double length() {return Math.sqrt(x*x+y*y+z*z);}
	public void scale(double s){
		x=x*s;
		y=y*s;
		z=z*s;
	}
}
