package balistyka;

public class CustomBullet extends Bullet implements ModelG7, ModelG1{
	public double initialVelocity;
	public double dragCoefficient;
	public double mass;
	public String name;
	@Override
	public void step1m(){            
		Vector v0;                                                       
		double dt;
		
		v0 = vel;
		
		E = dragCoefficient*E;                         //E koncowa
		vel.scale(Math.sqrt(2000*E/mass)/v0.length());          //predkosc koncowa
		dt = 2/(v0.x+vel.x);
		t = t + dt;              //czas
		
		vel.z = vel.z - 9.80665*dt;
		
		pos.x = pos.x + (vel.x+v0.x)/2*dt;
		pos.y = pos.y + (vel.y+v0.y)/2*dt;
		pos.z = pos.z + (vel.z+v0.z)/2*dt;
	}
	CustomBullet(String name, double mass, double dragCoefficient, double initialVelocity){
		super();
		this.name = name;
		this.mass = mass;
		this.dragCoefficient = dragCoefficient;
		this.initialVelocity = initialVelocity;
	}
	

	public void fire(double angle){
		double deg = angle/360*2*Math.PI;
		vel.x = initialVelocity*Math.cos(deg);
		vel.y = 0;
		vel.z = initialVelocity*Math.sin(deg);
				
		E = mass*vel.length()*vel.length()/2000;
	}
}
